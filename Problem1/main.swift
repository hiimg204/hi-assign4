//
//  main.swift
//  Problem1
//
//  Created by Student on 2016-10-18.
//  Copyright © 2016 Student. All rights reserved.
//
// Project:      assign-4-2016
// File:         Problem1.swift
// Author:       Ismael F. Hepp
// Date:         Oct 18, 2016
// Course:       IMG204
//
// Problem Statement:
// Design and implement a structure called Box that contains instance data
// that represents the height, width, and depth of the box. Also include a
// boolean computed variable called full as instance data that represents
// whether the box is  full or not. Define the Box initializer to accept and
// initialize the height, width, and depth of the box. Each newly created Box
// is empty. Include getter and  setter computed properties for all instance
// data. Include a "description" computed property that returns a one line
// description of the box.
//
// Use the "main.swift" belonging to Problem1 to create several Box objects
// and exercise all methods of the Box class.
//
// Remember that each class or structure should be stored in its own file.
//
// Inputs:   none
//
// Outputs:  representation of several Boxes from BoxTest driver,
//           i.e. output from the "description" method of class Box

import Foundation

var myBox: Box = Box(height: 20, width: 30, depth: 12)

print(myBox)

myBox.setFull(full: true)
myBox.setHeight(height: 30)
myBox.setWidth(width: 50)
myBox.setDepth(depth: 20)

print(myBox)
