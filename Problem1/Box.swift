//
//  Box.swift
//  hi-assign4
//
//  Created by Student on 2016-10-18.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

public struct Box : CustomStringConvertible {
  private var height: Double
  private var width: Double
  private var depth: Double
  private var full: Bool
  
  public init(height: Double, width: Double, depth: Double) {
    self.height = height
    self.width = width
    self.depth = depth
    full = false
  }
  
  public func getHeight() -> Double {
    return height
  }
  
  public func getWidth() -> Double {
    return width
  }
  
  public func getDepth() -> Double {
    return depth
  }
  
  public func isFull() -> Bool {
    return full
  }
  
  mutating public func setHeight(height: Double) {
    self.height = height
  }

  mutating public func setWidth(width: Double) {
    self.width = width
  }
  mutating public func setDepth(depth: Double) {
    self.depth = depth
  }
  mutating public func setFull(full: Bool) {
    self.full = full
  }
  
  public var description: String {
    var message : String  = "The box has: Height: \(height)m, "
    message += "Width: \(width)m, Depth: \(depth)m and is "
    if !self.isFull() {
      message += "not "
    }
    message += "full."
    return message
  }
  
}
