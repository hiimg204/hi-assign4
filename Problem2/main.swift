//
//  main.swift
//  Problem2
//
//  Created by Student on 2016-10-18.
//  Copyright © 2016 Student. All rights reserved.
//
// Project:      assign-4-2016
// File:         Problem2.swift
// Author:       Ismael F. Hepp
// Date:         Oct 18, 2016
// Course:       IMG204
//
// Problem Statement:
// Design and implement a class called PairOfDice, composed of two Die objects
// (this means that there will be at least two class files and a file for
// main.swift).
// Include methods to set and get computed properties the individual die 
// values, a method to roll the dice, and a method that returns the current 
// sum of the two die values.
// Create a method of the PairOfDice class that returns a tuple containing
// values of both dice.
//
// Use the "main.swift" belonging to Problem2 to instantiate and use a 
// PairOfDice object. Be sure to test all methods of both the Die and 
// PairOfDice classes.
//
//
// Inputs:
//
// Outputs:  output of all tests of the both Die and PairOfDice objects
//

import Foundation

var myDie1 : Die = Die()
print(myDie1.getValue())
myDie1.setValue()
print(myDie1.description)

var myDie2 : Die = Die()
print(myDie2.getValue())
myDie2.setValue()
print(myDie2.getValue())

var myPairOfDices : PairOfDice = PairOfDice()
print(myPairOfDices.description)
print(myPairOfDices.getValueDie())

myPairOfDices.rollDices()
print(myPairOfDices.description)

myPairOfDices.rollDie(number: 1)
print(myPairOfDices.description)

myPairOfDices.rollDie(number: 2)
print(myPairOfDices.description)

print(myPairOfDices.sumValues())


