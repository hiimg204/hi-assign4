//
//  PairOfDice.swift
//  hi-assign4
//
//  Created by Student on 2016-10-18.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class PairOfDice {
  private var die1: Die
  private var die2: Die
  
  public init() {
    die1 = Die()
    die2 = Die()
  }
  
  public func rollDices() {
    die1.roll()
    die2.roll()
  }
  
  public func rollDie(number: Int) {
    if number == 1 {
      die1.roll()
    } else if number == 2 {
      die2.roll()
    }
  }
  
  public func getValueDie() -> (Int, Int) {
    return (die1.getValue(), die2.getValue())
  }
  
  public func sumValues() -> Int {
    return die1.getValue() + die2.getValue()
  }
  
  public var description: String {
    var valueDie1: Int = die1.getValue()
    var valueDie2: Int = die2.getValue()
    var message = "Die 1 with face value of \(valueDie1). "
    message += "Die 2 with face value of \(valueDie2)."
    return message
  }
  
}
