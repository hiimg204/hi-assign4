//
//  Die.swift
//  hi-assign4
//
//  Created by Student on 2016-10-18.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

class Die {
  private var value: Int
  
  public init() {
    value = Int(arc4random_uniform(6) + 1 )
  }
  
  public func roll() {
    value = Int(arc4random_uniform(6) + 1 )
  }
  
  public func getValue() -> Int {
    return value
  }
  
  public func setValue() {
    self.roll()
  }
  
  public var description: String {
    return "Die with face value of \(value)."
  }
  
}
